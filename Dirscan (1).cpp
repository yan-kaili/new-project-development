#include <iostream>
#include <string>
#include <io.h>
#include <vector>	
#include<fstream>
#include <locale.h>
#include <Windows.h>

using namespace std;

#define PATH_SIZE 10000

typedef struct FileNode{
	string filename;    //文件名称
	bool isDir;        //是否是目录
	unsigned long long size; //文件大小
	time_t timestamp;     //文件时间戳
}FileNode;

typedef vector<FileNode> FileList;

string filename;    //文件名称
bool isDir;        //是否是目录
unsigned long long size; //文件大小
time_t timestamp;     //文件时间戳

fstream file;	//文件
char path[PATH_SIZE];
FileList fileList,behindList;
int nMode=-1;

FileList PrvFileList,CurFileList,DiffFileList;
FileNode data;

string front,behind;
int i;
char c;
int time; 


//截取字符串
vector<string> splitWithStl(const string str, const string part)
{
	vector<string> resVec;

	if ("" == str)//若为空，则返回原字符串
	{
		return resVec;
	}
	//方便截取最后一段数据
	string strs = str + part;

	size_t pos = strs.find(part);//find函数的返回值，若找到分隔符返回分隔符第一次出现的位置，
	//否则返回npos
	//此处用size_t类型是为了返回位置
	size_t size = strs.size();

	while (pos != string::npos)
	{
		string x = strs.substr(0, pos);//substr函数，获得子字符串
		resVec.push_back(x);
		strs = strs.substr(pos + 1, size);
		pos = strs.find(part);
	}
	return resVec;
}

void Connect(const char* str1, char* str2, char* p) { //字符串连接函数
	int i = 0;
	for (; *str1 != '\0';) {
		*p = *str1;
		str1++;
		p++;
	}
	for (; *str2 != '\0';) {
		*p = *str2;
		str2++;
		p++;
	}
	*p = '\0';
}


//nMode = FILE_OPTION_DIRECTORY 表示只取当前遍历文件夹下的所有文件夹 
int ScanDir(const char *sTopPath,FileList& fileList){
	//存放数据的结构体
	FileNode data;

	char b[5]="\\*.*";   // *.*代表全部类型的文件，类似的 *.txt则代表txt类型的文件
	char c[100],*p1,*p2;
	p1=c;
	Connect(sTopPath,b,p1);
	struct _finddata_t file;
	long fHandle;
	if( (fHandle=_findfirst(c, &file ))==-1L )
	{
		printf( "当前目录下没有文件\n");
		return 0;
	}
	else
		do{
			if (file.attrib & _A_SUBDIR)
			{
				//判断是否为"."当前目录，".."上一层目录
				if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0))
				{
					char newPath[100];
					p2=newPath;
					Connect(sTopPath,"\\",p2);//函数执行完成后p2仍指向newPath[0]
					for(int i=0;newPath[i]!='\0';i++){p2++;}//移动指针
					Connect("",file.name,p2);
					cout<< "文件夹名称："<<file.name <<endl;
					data.filename = file.name;
					data.isDir = true;
					data.size = file.size;
					data.timestamp = file.time_write;
					fileList.push_back(data);
					
					cout << "文件夹路径："<< newPath<<endl; 
					
					ScanDir(newPath,fileList);//递归调用
				}
			}
			else{
				continue;
			}
		}while( _findnext(fHandle,&file)==0 );
	_findclose( fHandle );
	return 1;	
}



//nMode = FILE_OPTION_FILE      表示只取当前遍历文件夹下的所有文件
int ScanFile(const char *sTopPath,FileList& fileList){
	//存放数据的结构体
	FileNode data;

	char b[5]="\\*.*";   // *.*代表全部类型的文件，类似的 *.txt则代表txt类型的文件
	char c[100],*p1,*p2;
	p1=c;
	Connect(sTopPath,b,p1);
	struct _finddata_t file;
	long fHandle;
		
	if( (fHandle=_findfirst(c, &file ))==-1L )
	{
		printf( "当前目录下没有文件\n");
		return 0;
	}
	else
		do{
			if (file.attrib & _A_SUBDIR)
			{
				//判断是否为"."当前目录，".."上一层目录
				if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0))
				{
					char newPath[100];
					p2=newPath;
					Connect(sTopPath,"\\",p2);//函数执行完成后p2仍指向newPath[0]
					for(int i=0;newPath[i]!='\0';i++){p2++;}//移动指针
					Connect("",file.name,p2);
					ScanFile(newPath,fileList);//递归调用
				}
			}
			else{
				data.filename = file.name;
				data.isDir = false;
				data.size = file.size;
				data.timestamp = file.time_write;
				fileList.push_back(data);
				cout<< "文件名称："<<file.name <<endl;
				
				cout << "文件路径："<< sTopPath<<endl; 

			}
		}while( _findnext(fHandle,&file)==0 );
	_findclose( fHandle );
	return 1;
} 



//nMode = FILE_OPTION_LIST      表示只取当前文件夹一层的文件夹 
int ScanOneDir(const char *sTopPath,FileList& fileList){
		//存放数据的结构体
	FileNode data;

	char b[5]="\\*.*";   // *.*代表全部类型的文件，类似的 *.txt则代表txt类型的文件
	char c[100],*p1,*p2;
	p1=c;
	Connect(sTopPath,b,p1);
	struct _finddata_t file;
	long fHandle;
		
	if( (fHandle=_findfirst(c, &file ))==-1L )
	{
		printf( "当前目录下没有文件\n");
		return 0;
	}
	else
		do{
			if (file.attrib & _A_SUBDIR)
			{
				
				//判断是否为"."当前目录，".."上一层目录
				if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0))
				{
					cout<< "文件夹名称："<<file.name <<endl;
					data.filename = file.name;
					data.isDir = true;
					data.size = file.size;
					data.timestamp = file.time_write;
					
					cout << "文件夹路径："<<sTopPath<<endl;
					fileList.push_back(data);
										
				}
			}
			else{
				continue;

			}
		}while( _findnext(fHandle,&file)==0 );
	_findclose( fHandle );
	return 1;
} 



int ScanDirFile(const char *sTopPath,FileList& fileList){
		//存放数据的结构体
	FileNode data;

	char b[5]="\\*.*";   // *.*代表全部类型的文件，类似的 *.txt则代表txt类型的文件
	char c[100],*p1,*p2;
	p1=c;
	Connect(sTopPath,b,p1);
	struct _finddata_t file;
	long fHandle;
		
	if( (fHandle=_findfirst(c, &file ))==-1L )
	{
		printf( "当前目录下没有文件\n");
		return 0;
	}
	else
		do{
			if (file.attrib & _A_SUBDIR)
			{
				//判断是否为"."当前目录，".."上一层目录
				if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0))
				{
					char newPath[100];
					p2=newPath;
					Connect(sTopPath,"\\",p2);//函数执行完成后p2仍指向newPath[0]
					for(int i=0;newPath[i]!='\0';i++){p2++;}//移动指针
					Connect("",file.name,p2);
					cout<< "文件夹名称："<<file.name <<endl;
					data.filename = file.name;
					data.isDir = true;
					data.size = file.size;
					data.timestamp = file.time_write;
					fileList.push_back(data);
					cout << "文件夹路径："<<newPath<<endl;
				}
			}
			else{
				data.filename = file.name;
				data.isDir = false;
				data.size = file.size;
				data.timestamp = file.time_write;
				fileList.push_back(data);
				cout<< "文件名称："<<file.name <<endl;
				cout<< "文件路径："<<sTopPath<<endl;
			}
		}while( _findnext(fHandle,&file)==0 );
	_findclose( fHandle );
	return 1; 
}



//nMode = FILE_OPTION_DIRECTORY | FILE_OPTION_FILE  表示取当前遍历文件夹下所有的文件和文件夹 
int ScanAll(const char *sTopPath,FileList& fileList){
	//存放数据的结构体
	FileNode data;

	char b[5]="\\*.*";   // *.*代表全部类型的文件，类似的 *.txt则代表txt类型的文件
	char c[100],*p1,*p2;
	p1=c;
	Connect(sTopPath,b,p1);
	struct _finddata_t file;
	long fHandle;
		
	if( (fHandle=_findfirst(c, &file ))==-1L )
	{
		printf( "当前目录下没有文件\n");
		return 0;
	}
	else
		do{
			if (file.attrib & _A_SUBDIR)
			{
				//判断是否为"."当前目录，".."上一层目录
				if ((strcmp(file.name, ".") != 0) && (strcmp(file.name, "..") != 0))
				{
					char newPath[100];
					p2=newPath;
					Connect(sTopPath,"\\",p2);//函数执行完成后p2仍指向newPath[0]
					for(int i=0;newPath[i]!='\0';i++){p2++;}//移动指针
					Connect("",file.name,p2);
					cout<< "文件夹名称："<<file.name <<endl;
					data.filename = file.name;
					data.isDir = true;
					data.size = file.size;
					data.timestamp = file.time_write;
					fileList.push_back(data);

					cout << "文件夹路径："<< newPath<<endl;

					ScanAll(newPath,fileList);//递归调用
				}
			}
			else{
				data.filename = file.name;
				data.isDir = false;
				data.size = file.size;
				data.timestamp = file.time_write;
				fileList.push_back(data);
				cout<< "文件名称："<<file.name <<endl;
				
				cout << "文件路径："<<sTopPath<<endl; 
			}
		}while( _findnext(fHandle,&file)==0 );
	_findclose( fHandle );
	return 1; 
}

 
//sTopPath:需要遍历的文件夹
//fileList: 存放遍历的文件列表结果集
//nMode:表示按某种模式去遍历指定目录
int DirScan(const char *sTopPath,FileList& fileList, int nMode){
	int num=0;
	switch(nMode){
		case 1:
			cout<< "提取所有文件夹信息"<<endl; 
			num = ScanDir(sTopPath,fileList);break;
		case 2:
			cout<< "提取所有文件信息"<<endl; 
			num = ScanFile(sTopPath,fileList);break;	
		case 3:
			cout<< "提取当前层目录文件夹信息"<<endl; 
			num = ScanOneDir(sTopPath,fileList);break;						
		case 4:
			cout<< "提取所有文件夹和文件信息"<<endl; 
			num = ScanAll(sTopPath,fileList);break;						
		case 5:
			cout<< "提取当前目层录文件夹和文件信息"<<endl; 
			num = ScanDirFile(sTopPath,fileList);break;
		default:
			cout<<"输入错误"<<endl;break; 
	}

	

	return num;
	
	
}

//信息比对
int DirScanDirff(const FileList&PrvFileList,const FileList & CurFileList, FileList & DiffFileList){
	FileNode data;
	bool x=false;
	
	string filename;    //文件名称
	bool isDir;        //是否是目录
	unsigned long long size; //文件大小
	time_t timestamp;     //文件时间戳

	cout<<"-----------------------------------"<<endl;
	//B中有，A中没有的记为 FILE_ADD
	if(PrvFileList.size()<CurFileList.size()){		//后遍历比前一次多 
	    
		for(int i=0;i<CurFileList.size();i++){		//循环输出后一次信息 
		    x = false; 
			for(int j=0;j<PrvFileList.size();j++){	//循环输出前一次信息	
				if((PrvFileList[j].filename == CurFileList[i].filename) && (PrvFileList.size()!= j) ){		//若名称相等则不是增添的文件或文件夹 
					//CurFileList.erase(CurFileList[i]);						//移除
					x = true; 													//跳出标志位 
					break;
				}
			
			}
			if(x!=true){
				data.filename = CurFileList[i].filename;
				data.isDir = CurFileList[i].isDir;
				data.size = CurFileList[i].size;
				data.timestamp = CurFileList[i].timestamp;
				DiffFileList.push_back(data);				
			} 
			x = false;
						
		}
		return 1;
	}
	//B中有，A中没有的,若时间不一致，大小不一致的记为 FILE_MODIFY	
	else if (PrvFileList.size()==CurFileList.size()){
	
		for (int i=0;i<PrvFileList.size();i++)
		{	
		    x = false;
			for(int j=0;j<CurFileList.size();j++){
				if((PrvFileList[i].filename == CurFileList[j].filename) && (PrvFileList.size()!= j) &&( PrvFileList[i].timestamp!=CurFileList[j].timestamp)){		//若名称相等时间戳不等，则被修改 
					data.filename = CurFileList[i].filename;
					data.isDir = CurFileList[i].isDir;
					data.size = CurFileList[i].size;
					data.timestamp = CurFileList[i].timestamp;
					DiffFileList.push_back(data);
					x = true;
				}				
			}		
			
		}
		if(x == true){
			return 2;
		}else{
			return 0;
		}
			
		
	}
	//B中没有，A中有的记为 FILE_DELETE
	else if(PrvFileList.size()>CurFileList.size()){

		for(int i=0;i<PrvFileList.size();i++){		//循环输出前一次信息 
		    x = false;
			for(int j=0;j<CurFileList.size();j++){	//循环输出后一次信息	
				if((PrvFileList[i].filename == CurFileList[j].filename) && (j != CurFileList.size()-1) ){		//若名称相等则不是增添的文件或文件夹 
					//CurFileList.erase(CurFileList[i]);						//移除
					x = true; 													//跳出标志位 
					break;
				}
				if(x!=true && (j == CurFileList.size()-1)){
				data.filename = PrvFileList[i].filename;
				data.isDir = PrvFileList[i].isDir;
				data.size = PrvFileList[i].size;
				data.timestamp = PrvFileList[i].timestamp;
				DiffFileList.push_back(data);	
							
			} 
				
			}
			
			if(x!=true){
				data.filename = PrvFileList[i].filename;
				data.isDir = PrvFileList[i].isDir;
				data.size = PrvFileList[i].size;
				data.timestamp = PrvFileList[i].timestamp;
				DiffFileList.push_back(data);	
							
			} 
			x = false;
						
		}
		return 3;		
	}

	
	return 0;
	cout<<"-----------------------------------"<<endl;
	
	
	//return 0;
}
//前一次遍历
void frontScan(){
	
	//DirScan(path,fileList,nMode);
	while(!(DirScan(path,fileList,nMode))){
		cout<< "请输入目录"<<endl;
		scanf("%s",path);    //输入你要遍历的目录
		cout<< "请输入遍历方式"<<endl;
		cout<<"1:   FILE_OPTION_DIRECTORY = 0x10                只取目录" << endl;
	    cout<<"2:   FILE_OPTION_FILE = 0x20,                    只取文件" << endl;
		cout<<"3:   FILE_OPTION_LIST|FILE_OPTION_DIRECTORY      表示只取当前文件夹一层目录" << endl;
		cout<<"4:   FILE_OPTION_DIRECTORY | FILE_OPTION_FILE    表示取文件和目录"<<endl;
		cout<<"5:   FILE_OPTION_DIRECTORY | FILE_OPTION_FILE | FILE_OPTION_LIST 	表示取第一层文件和目录" << endl;
		scanf("%d",&nMode); 
		cout<<"设置定时/秒"<<endl; 
		scanf("%d",&time); 
		cout<<endl;
		
		
	}
	
	file.open("result.txt",ios::out); //以只写模式打开文件

	for(std::vector<FileNode>::iterator it=fileList.begin();it!=fileList.end();it++)
	{
		filename = (*it).filename;
		isDir = (*it).isDir;
		size = (*it).size;
		timestamp = (*it).timestamp;
		//cout << filename<<endl;
		file << "$"<< filename <<"$"<< isDir << "$"<< size <<"$"<<timestamp <<endl;
	}

	file.close(); //关闭文件
}

//后遍历
void behindScan(){
	DirScan(path,behindList,nMode);

	file.open("result.txt",ios::out); //以只写模式打开文件

	for(std::vector<FileNode>::iterator it=behindList.begin();it!=behindList.end();it++)
	{
		filename = (*it).filename;
		isDir = (*it).isDir;
		size = (*it).size;
		timestamp = (*it).timestamp;
		//cout << filename<<endl;
		file << "$"<< filename <<"$"<< isDir << "$"<< size <<"$"<<timestamp <<endl;
	}

	file.close(); //关闭文件
}



int main()
{	
	cout<< "请输入目录"<<endl;
	scanf("%s",path);    //输入你要遍历的目录
	cout<< "请输入遍历方式"<<endl;
	cout<<"1:   FILE_OPTION_DIRECTORY = 0x10                只取目录" << endl;
    cout<<"2:   FILE_OPTION_FILE = 0x20,                    只取文件" << endl;
	cout<<"3:   FILE_OPTION_LIST|FILE_OPTION_DIRECTORY      表示只取当前文件夹一层目录" << endl;
	cout<<"4:   FILE_OPTION_DIRECTORY | FILE_OPTION_FILE    表示取文件和目录"<<endl;
	cout<<"5:   FILE_OPTION_DIRECTORY | FILE_OPTION_FILE | FILE_OPTION_LIST 	表示取第一层文件和目录" << endl;
	scanf("%d",&nMode); 
	cout<<"设置定时/秒"<<endl; 
	scanf("%d",&time); 
	cout<<endl;
	//首次，前遍历
	frontScan();

	while(1){

		//文件打开1	
		freopen("result.txt","r",stdin);
		while(scanf("%c",&c)!=EOF){
			front+=c;	
		}	
		fclose(stdin);

		//本次信息读取
		vector<string> prvFileList=splitWithStl(front, "$");
		for (i = 1; i < prvFileList.size(); i++)
		{

			data.filename = prvFileList[i];
			//cout<<data.filename<<endl;

			i+=1;	//向后移动
			(prvFileList[i] == "1" )? (data.isDir = true) : (data.isDir = false);
			//cout <<data.isDir<<endl;

			i+=1;
			data.size =stoll(prvFileList[i]);		//stoll 将字符串转long long 
			//cout<<data.size<<endl;

			i+=1;
			data.timestamp = stol(prvFileList[i]);	//stoll 将字符串转long
			//cout<<data.timestamp<<endl;

			PrvFileList.push_back(data);	
		}



//后遍历
		behindScan();

//文件打开2
	freopen("result.txt","r",stdin);
	while(scanf("%c",&c)!=EOF){
		behind+=c;
	}
	fclose(stdin);	
	//下一次信息读取
	vector<string> curFileList=splitWithStl(behind, "$");
	for (i = 1; i < curFileList.size(); i++)
	{

		data.filename = curFileList[i];
		//cout<<data.filename<<endl;

		i+=1;	//向后移动
		(curFileList[i] == "1" )? (data.isDir = true) : (data.isDir = false);
		//cout <<data.isDir<<endl;

		i+=1;
		data.size =stoll(curFileList[i]);		//stoll 将字符串转long long 
		//cout<<data.size<<endl;

		i+=1;
		data.timestamp = stol(curFileList[i]);	//stoll 将字符串转long
		//cout<<data.timestamp<<endl;

		CurFileList.push_back(data);	
	}
	//信息比对
	int x = DirScanDirff(PrvFileList,CurFileList,DiffFileList);
	switch(x){
		case 0:
			cout<<"YES"<<endl;break;
		case 1:{
			//打印提示信息 
			cout<<"FILE_ADD"<<endl;
				file.open("diff.txt",ios::app); //以只写模式打开文件

				for(std::vector<FileNode>::iterator it=DiffFileList.begin();it!=DiffFileList.end();it++)
				{
					filename = (*it).filename;
					isDir = (*it).isDir;
					size = (*it).size;
					timestamp = (*it).timestamp;
					//cout << filename<<endl;
					file << "$"<< filename <<"$"<< isDir << "$"<< size <<"$"<<timestamp << "$"<<"FILE_ADD"<<endl;
				}

				file.close(); //关闭文件
				DiffFileList.clear();			
			break;
		}
			
		case 2:
			{
				cout<<"FILE_MODIFY"<<endl; 
				file.open("diff.txt",ios::app); //以只写模式打开文件

				for(std::vector<FileNode>::iterator it=DiffFileList.begin();it!=DiffFileList.end();it++)
				{
					filename = (*it).filename;
					isDir = (*it).isDir;
					size = (*it).size;
					timestamp = (*it).timestamp;
					//cout << filename<<endl;
					file << "$"<< filename <<"$"<< isDir << "$"<< size <<"$"<<timestamp << "$"<<"FILE_MODIFY"<<endl;
				}

				file.close(); //关闭文件
				DiffFileList.clear();
			break;
			}
			
		case 3:{
			//打印提示信息 
			cout<< "FILE_DELETE"<<endl;
			
				file.open("diff.txt",ios::app); //以只写模式打开文件

				for(std::vector<FileNode>::iterator it=DiffFileList.begin();it!=DiffFileList.end();it++)
				{
					filename = (*it).filename;
					isDir = (*it).isDir;
					size = (*it).size;
					timestamp = (*it).timestamp;
					//cout << filename<<endl;
					file << "$"<< filename <<"$"<< isDir << "$"<< size <<"$"<<timestamp << "$"<<"FILE_DELETE"<<endl;
				}

				file.close(); //关闭文件
				DiffFileList.clear();		
			break;
		}		


	}


	

	//字符串清零
	front = "";
	behind = "";
	//清除vector
	fileList.clear();
	behindList.clear();

//再遍历
	frontScan();
	Sleep(time*1000);
	

	
	}
	system("pause");
	return 0;

	
}

